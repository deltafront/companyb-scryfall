package companyB.scryfall.annotations;

public class UntestedAnnotationsProcessor extends BaseAnnotationProcesor<Untested> {
    public UntestedAnnotationsProcessor() {
        super(Untested.class, "This method is untested");
    }
}
