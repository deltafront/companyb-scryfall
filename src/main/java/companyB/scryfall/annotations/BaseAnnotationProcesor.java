package companyB.scryfall.annotations;

import javax.annotation.processing.AbstractProcessor;
import javax.annotation.processing.RoundEnvironment;
import javax.lang.model.SourceVersion;
import javax.lang.model.element.Element;
import javax.lang.model.element.TypeElement;
import java.lang.annotation.Annotation;
import java.util.Collections;
import java.util.Set;
import static javax.tools.Diagnostic.Kind.WARNING;

public class BaseAnnotationProcesor<T extends Annotation> extends AbstractProcessor {

    private final Class<T> annotationClass;
    private final String message;

    public BaseAnnotationProcesor(Class<T> annotationClass, String message) {
        this.annotationClass = annotationClass;
        this.message = message;
    }

    @Override public SourceVersion getSupportedSourceVersion() {
        return SourceVersion.latestSupported();
    }

    @Override public Set<String> getSupportedAnnotationTypes() {
        return Collections.singleton(annotationClass.getName());
    }

    @Override public boolean process(Set<? extends TypeElement> annotations, RoundEnvironment roundEnv) {
        final TypeElement typeElement = processingEnv.getElementUtils().getTypeElement(annotationClass.getCanonicalName());
        final Set<? extends Element> elements = roundEnv.getElementsAnnotatedWith(typeElement);

        for (Element element : elements) {
            processingEnv.getMessager().printMessage(WARNING, message, element);
        }
        return true;
    }
}
