package companyB.scryfall.analytics

import companyB.scryfall.model.Ruling
import companyB.scryfall.model.ScryfallCard

object CollectionsAnalytics{


    private fun groupByRarity(cards: List<ScryfallCard>): Map<String, List<ScryfallCard>> = groupBySingleValue(cards){ card -> card.rarity}

    private fun groupByManaCost(cards: List<ScryfallCard>): Map<String, List<ScryfallCard>> = groupBySingleValue(cards){ card -> card.mana_cost}

    private fun groupByCmc(cards: List<ScryfallCard>): Map<Double, List<ScryfallCard>> = groupBySingleValue(cards){ card -> card.cmc}

    private fun groupByPower(cards: List<ScryfallCard>): Map<String, List<ScryfallCard>> = groupBySingleValue(cards){ card -> card.power}

    private fun groupByToughness(cards: List<ScryfallCard>): Map<String, List<ScryfallCard>> = groupBySingleValue(cards){ card -> card.toughness}

    private fun groupByType(cards: List<ScryfallCard>): Map<String, List<ScryfallCard>> = groupBySingleValue(cards){ card -> card.type_line}

    private fun groupByColors(cards: List<ScryfallCard>): Map<Array<String>, List<ScryfallCard>> =
        groupByList(cards){ card -> card.colors}

    private fun groupByColorIdentity(cards: List<ScryfallCard>): Map<Array<String>, List<ScryfallCard>> =
        groupByList(cards){ card -> card.color_identity}

    private fun groupByLegality(cards: List<ScryfallCard>): Map<String, MutableList<ScryfallCard>> {
        val legalities = cards.flatMap { card -> card.legalities.keys.filter { Ruling.legal == Ruling.valueOf(card.legalities.getValue(it)) } }
        val mappings: Map<String, MutableList<ScryfallCard>> = legalities.map { it to mutableListOf<ScryfallCard>() }.toMap()
        cards.forEach{card ->
            legalities.forEach{legality ->
                if(  Ruling.legal == Ruling.valueOf(card.legalities.getValue(legality)))
                    mappings.getValue(legality).add(card)
            }
        }
        return mappings
    }


    private fun <T>groupBySingleValue(cards: List<ScryfallCard>, func: (ScryfallCard)-> T): Map<T, List<ScryfallCard>> =
        cards.groupBy(func)

    private fun<T>groupByList(cards: List<ScryfallCard>, func: (ScryfallCard)-> Array<T>): Map<Array<T>, List<ScryfallCard>> =
        cards.groupBy { card -> func(card)}



}