package companyB.scryfall

object Constants {
    val pwd = "${System.getProperty("user.dir")}/resources"
    val cardCacheFile = "$pwd/cards.json"
    val autoCompletionCacheFile = "$pwd/autocompletion.json"
    val catalogCacheFile = "$pwd/catalogues.json"
    const val apiBasePath: String = "https://api.scryfall.com"
    val headers: Map<String,String> = mapOf("Accept" to "application/json")
    val cacheExpiryFile: String = "$pwd/cache-expiry.json"

}