package companyB.scryfall.utility

import java.lang.reflect.Field

object ReflectionUtility {
    fun getFields(instance: Any): List<Field> = instance::class.java.declaredFields.toList()

    fun getFieldValue(instance: Any, field: Field): Any? {
        field.isAccessible = true
        return field.get(instance)
    }
}