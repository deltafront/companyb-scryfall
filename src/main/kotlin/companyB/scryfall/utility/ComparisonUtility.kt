package companyB.scryfall.utility

import java.lang.IllegalStateException
import java.lang.reflect.Field

object ComparisonUtility {

    fun compareValues(first: Any, second: Any): Boolean {
        return if (first::class.java.equals(second::class.java)) {
            Attempt {
                ReflectionUtility.getFields(first).forEach { field ->
                    if (!fieldsAreEqual(field, first, second))
                        throw IllegalStateException()
                }
            }.succeeded()
        } else false
    }

    private fun fieldsAreEqual(field: Field, first: Any, second: Any): Boolean {
        return if (first::class.java.equals(second::class.java)) {
            val firstFieldValue: Any? = ReflectionUtility.getFieldValue(first, field)
            val secondFieldValue: Any? = ReflectionUtility.getFieldValue(second, field)
            if (firstFieldValue is Array<*> && secondFieldValue is Array<*>) {
                arraysAreEqual(firstFieldValue, secondFieldValue)
            } else if (firstFieldValue is Map<*, *> && secondFieldValue is Map<*, *>) {
                mapsAreEqual(firstFieldValue, secondFieldValue)
            } else firstFieldValue == secondFieldValue
        } else false
    }

    private fun arraysAreEqual(firstArray: Array<*>, secondArray: Array<*>): Boolean =
        firstArray.containsAll(secondArray)

    private fun mapsAreEqual(firstMap: Map<*, *>, secondMap: Map<*, *>): Boolean =
        firstMap.containsAll(secondMap)


}

fun Array<*>.containsAll(array: Array<*>): Boolean{
    return if(this.size == array.size){
        this.count { array.contains(it) } == this.size
    }
    else false
}

fun Map<*,*>.containsAll(map: Map<*,*>): Boolean {
    return if(this.size == map.size){
        this.keys.count { this[it] == map[it]} == this.size
    }
    else false
}
