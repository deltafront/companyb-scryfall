package companyB.scryfall.utility

import com.beust.klaxon.Klaxon
import java.time.LocalDate

object CacheFileExpirationUtility {
    private val now = LocalDate.now()
    private val klaxon: Klaxon = Klaxon()
    //TODO - read from file, write to file
    fun isPastDate(date: String): Boolean = now.isAfter(LocalDate.parse(date))

    // parse in `YYYY-MM-DD` format
    fun generateNextExpiryDate(): String {
        val expiry: Expiry = getNextExpiry()
        return "${expiry.year}-${expiry.month}-01"
    }
    fun serializeExpiry(expiry: Expiry): String =
        klaxon.toJsonString(expiry)

    fun deserializeExpiry(expiryString: String): Expiry? =
        klaxon.parse<Expiry>(expiryString)

    fun cacheShouldExpire(expiry: Expiry): Boolean =
        !getCurrentExpiry().greaterThan(expiry)


    private fun getCurrentExpiry(): Expiry =
        Expiry(now.month.value, now.year)

    private fun getNextExpiry():Expiry  {
        val currentMonth: Int = now.month.value
        val currentYear: Int = now.year
       return when(currentMonth){
            1,2,3 -> Expiry(4,currentYear)
            4,5,6 -> Expiry(7,currentYear)
            7,8,9 -> Expiry(10,currentYear)
            else -> Expiry(1,currentYear+1)
        }
    }

}
data class Expiry(val month: Int, val year: Int){
    fun greaterThan(other: Expiry): Boolean =
        other.month > this.month || other.year > this.year


    override fun equals(other: Any?): Boolean {
        return if (other != null && other is Expiry){
            other.month == this.month &&
                    other.year == this.year
        }
        else false
    }
}