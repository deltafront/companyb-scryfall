package companyB.scryfall.utility

/**
 * Largely adapted from https://dzone.com/articles/kotlin-try-type-for-functional-exception-handling with some improvements.
 */
@Suppress("UNCHECKED_CAST")
sealed class Attempt<out T>(private val succeeded: Boolean, private val input: T?, private val exception: Throwable?) {
    fun succeeded(): Boolean  = succeeded
    fun failed(): Boolean = !succeeded
    fun get(): T  = if(succeeded) input!! else throw exception!!
    fun getOrElse(default: @UnsafeVariance T): T = if(succeeded) input!! else default
    fun orElse(default: Attempt<@UnsafeVariance T>): Attempt<T> = if(succeeded) this else default

    companion object {
        operator fun <T> invoke(function: () -> T): Attempt<T> {
            return try {
                Success(function())
            } catch (e: Exception) {
                Failure(e)
            }
        }
    }
    fun <U> map(f: (T) -> U): Attempt<U> {
        return when (this) {
            is Success -> Attempt{
                f(this.input)
            }
            is Failure -> this as Failure<U>
        }
    }

    fun <U> flatMap(f: (T) -> Attempt<U>): Attempt<U> {
        return when (this) {
            is Success -> f(this.input)
            is Failure -> this as Failure<U>
        }
    }
}

data class Success<out T>(val input: T): Attempt<T>(true, input, null)

data class Failure<out T>(val exception: Throwable): Attempt<T>(false, null, exception)