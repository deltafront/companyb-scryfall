package companyB.scryfall.utility

import com.beust.klaxon.Klaxon
import companyB.scryfall.model.*

object ScryfallCardJsonUtility {
    private val klaxon: Klaxon = Klaxon()

    fun serializeScryfallCard(scryfallCard: ScryfallCard): String =
        klaxon.toJsonString(scryfallCard)

    fun serializeAutocmopletionCatlogueEntry(autoCompletionCatlogueEntry: AutoCompletionCatlogueEntry): String =
            klaxon.toJsonString(autoCompletionCatlogueEntry)

    fun serializeCatlogue(catlogue: Catalogue): String =
        klaxon.toJsonString(catlogue)

    fun deserializeScryfallCard(jsonString: String): ScryfallCard? =
        klaxon.parse<ScryfallCard>(jsonString)

    fun deserializeScryfallCards(jsonString: String): ScryfallCards? =
            klaxon.parse<ScryfallCards>(jsonString)

    fun deserializeCatlogue(jsonString: String): Catalogue? =
            klaxon.parse<Catalogue>(jsonString)

    fun deserialzeAutocomplettionCatalogueEntry(jsonString: String): AutoCompletionCatlogueEntry? =
            klaxon.parse<AutoCompletionCatlogueEntry>(jsonString)

    fun deserialzeAutocomplettionCatalogue(jsonString: String): AutoCompletionCatlogue? =
        klaxon.parse<AutoCompletionCatlogue>(jsonString)


}