package companyB.scryfall.utility

object HashCodeUtility {

    fun hashCode(instance: Any): Int =
        ReflectionUtility.getFields(instance).map {
            val value = ReflectionUtility.getFieldValue(instance, it)
            when (value) {
                is Array<*> -> value.map { v -> v.hashCode() }.map { it }.map { it }.sum()
                is Map<*,*> -> value.keys.map { k -> value[k].hashCode() }.map{it}.sum()
                else -> ReflectionUtility.getFieldValue(instance, it).hashCode()
            }
        }.sum()

}