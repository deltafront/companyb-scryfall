package companyB.scryfall.api

import java.net.URLEncoder

/**
 * For all implementations, the following caveats apply:
 *  <ol>
 *      <li>Pretty Printing is not supported</li>
 *      <li>Only Json format is supported</li>
 *  </ol>
 */
abstract class BaseScryfallApi{

    protected fun buildUrl(baseUrl: String, vararg parameterTuples: ParameterTuple): String {
        val additionalValues: String = parameterTuples.filter { parameterTuple -> null != parameterTuple.value }
            .map { parameterTuple -> "${parameterTuple.name}=${parameterTuple.value}" }
            .joinToString { "$it&" }
        return "$baseUrl?${additionalValues.substring(0, additionalValues.lastIndexOf('&'))}"

    }
    protected fun encode(url: String) = URLEncoder.encode(url, "UTF-8")
}
data class ParameterTuple(val name: String, val value: Any?)
