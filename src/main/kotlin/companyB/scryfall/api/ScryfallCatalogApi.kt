package companyB.scryfall.api

import companyB.scryfall.Constants
import companyB.scryfall.Constants.apiBasePath
import companyB.scryfall.cache.CatalogueEntryCache
import companyB.scryfall.model.Catalogue
import companyB.scryfall.utility.ScryfallCardJsonUtility
import khttp.responses.Response


enum class CatalogueType{
    `card-names`, `artist-names`, `word-bank`,
    `creature-types`,`planeswalker-types`,`land-types`,
    `artifact-types`,`enchantment-types`,`spell-types`,
    powers,toughness,loyalties,watermarks

}

object ScryfallCatalogApi: BaseScryfallApi() {
    private const val baseCatalogPath: String = "$apiBasePath/catalog"

    /**
     * Please consult https://scryfall.com/docs/api/catalogs/card-names
     * @param find_in_cache Whether to look in the cache for a result before making a request to Scryfall. The default value is `true`.
     */
    fun getCardNames(find_in_cache: Boolean = true) = getCatalogResponse(CatalogueType.`card-names`, find_in_cache)

    /**
     * Please consult https://scryfall.com/docs/api/catalogs/artist-names
     * @param find_in_cache Whether to look in the cache for a result before making a request to Scryfall. The default value is `true`.
     */
    fun getArtistNames(find_in_cache: Boolean = true) = getCatalogResponse(CatalogueType.`artist-names`, find_in_cache)

    /**
     * Please consult https://scryfall.com/docs/api/catalogs/word-bank
     * @param find_in_cache Whether to look in the cache for a result before making a request to Scryfall. The default value is `true`.
     */
    fun getWordBank(find_in_cache: Boolean = true) = getCatalogResponse(CatalogueType.`word-bank`, find_in_cache)

    /**
     * Please consult https://scryfall.com/docs/api/catalogs/creature-types
     * @param find_in_cache Whether to look in the cache for a result before making a request to Scryfall. The default value is `true`.
     */
    fun getCreatureTypes(find_in_cache: Boolean = true) = getCatalogResponse(CatalogueType.`creature-types`, find_in_cache)

    /**
     * Please consult https://scryfall.com/docs/api/catalogs/land-types
     * @param find_in_cache Whether to look in the cache for a result before making a request to Scryfall. The default value is `true`.
     */
    fun getLandTypes(find_in_cache: Boolean = true) = getCatalogResponse(CatalogueType.`land-types`, find_in_cache)

    /**
     * Please consult https://scryfall.com/docs/api/catalogs/planeswalker-types
     * @param find_in_cache Whether to look in the cache for a result before making a request to Scryfall. The default value is `true`.
     */
    fun getPlansewalkerTypes(find_in_cache: Boolean = true) = getCatalogResponse(CatalogueType.`planeswalker-types`, find_in_cache)

    /**
     * Please https://scryfall.com/docs/api/catalogs/artifact-types
     * @param find_in_cache Whether to look in the cache for a result before making a request to Scryfall. The default value is `true`.
     */
    fun getArtifactTypes(find_in_cache: Boolean = true) = getCatalogResponse(CatalogueType.`artifact-types`, find_in_cache)

    /**
     * Please consult https://scryfall.com/docs/api/catalogs/enchantment-types
     * @param find_in_cache Whether to look in the cache for a result before making a request to Scryfall. The default value is `true`.
     */
    fun getEnchantmentTypes(find_in_cache: Boolean = true) = getCatalogResponse(CatalogueType.`enchantment-types`, find_in_cache)

    /**
     * Please consult https://scryfall.com/docs/api/catalogs/spell-types
     * @param find_in_cache Whether to look in the cache for a result before making a request to Scryfall. The default value is `true`.
     */
    fun getSpellTypes(find_in_cache: Boolean = true) = getCatalogResponse(CatalogueType.`spell-types`, find_in_cache)

    /**
     * Please consult https://scryfall.com/docs/api/catalogs/powers
     * @param find_in_cache Whether to look in the cache for a result before making a request to Scryfall. The default value is `true`.
     */
    fun getPowers(find_in_cache: Boolean = true) = getCatalogResponse(CatalogueType.powers, find_in_cache)

    /**
     * Please consult https://scryfall.com/docs/api/catalogs/toughnesses
     * @param find_in_cache Whether to look in the cache for a result before making a request to Scryfall. The default value is `true`.
     */
    fun getToughness(find_in_cache: Boolean = true) = getCatalogResponse(CatalogueType.toughness, find_in_cache)

    /**
     * Please consult https://scryfall.com/docs/api/catalogs/watermarks
     * @param find_in_cache Whether to look in the cache for a result before making a request to Scryfall. The default value is `true`.
     */
    fun getWatermarks(find_in_cache: Boolean = true) = getCatalogResponse(CatalogueType.watermarks, find_in_cache)

    /**
     * Please consult https://scryfall.com/docs/api/catalogs/loyalties
     * @param find_in_cache Whether to look in the cache for a result before making a request to Scryfall. The default value is `true`.
     */
    fun getLoyalites(find_in_cache: Boolean = true) = getCatalogResponse(CatalogueType.loyalties, find_in_cache)

    private fun getCatalogResponse(catalogueType: CatalogueType, find_in_cache: Boolean = true): Catalogue? {
        val uri: String = "$baseCatalogPath/${catalogueType.name}"
        var catalog: Catalogue? = if (find_in_cache)
            CatalogueEntryCache.findInCache(uri) { catalogue -> catalogue!!.uri }
        else null
        if (null == catalog) {
            val resopnse: Response = khttp.get(
                url = encode(uri),
                headers = Constants.headers
            )
            catalog = ScryfallCardJsonUtility.deserializeCatlogue(resopnse.jsonObject.toString())
            CatalogueEntryCache.writeToCache(catalog!!)
        }
        return catalog
    }
}