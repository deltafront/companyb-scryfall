package companyB.scryfall.api

import companyB.scryfall.*
import companyB.scryfall.annotations.Untested
import companyB.scryfall.cache.AutocmopletionCatlogueEntryCache
import companyB.scryfall.cache.ScryfallCardCache
import companyB.scryfall.model.AutoCompletionCatlogue
import companyB.scryfall.model.AutoCompletionCatlogueEntry
import companyB.scryfall.model.ScryfallCard
import companyB.scryfall.model.ScryfallCards
import companyB.scryfall.utility.Attempt
import companyB.scryfall.utility.ScryfallCardJsonUtility
import khttp.responses.Response
import org.json.JSONArray
import org.json.JSONObject

enum class UniquenessStrategy{
    cards, art, prints
}


enum class Order{
    _name, set, released, rarity, color, usd, tix, eur,cmc, power, toughness, edrec, artist
}


enum class Direction {
    auto,asc,desc
}

enum class SearchType {
    exact, fuzzy
}
object ScryfallCardsApi : BaseScryfallApi() {
    private const val cardsPath: String = "${Constants.apiBasePath}/cards"
    private const val cardsSearchPath: String = "$cardsPath/search"
    private const val autoCompletionPath: String= "$cardsPath/autocomplete"
    private const val randomCardPath: String = "$cardsPath/random"
    private const val tgcpCardSearchPath: String = "$cardsPath/tcgplayer"
    private const val mtgoCardSearchPath: String = "$cardsPath/mtgo"
    private const val areaCardSearchPath: String = "$cardsPath/arena"
    private const val multiverseCardSearchPath: String = "$cardsPath/multiverse"

    /**
     * Gets all of the cards utilizing auto-pagination
     * @param load_in_cache  Whether or not to load results in cache. Defaults to `true`.
     */
    @Untested
    fun getAllCards(load_in_cache: Boolean = true): List<ScryfallCard> {
        val allCards: MutableList<ScryfallCard> = mutableListOf()
        var scryfallCard: ScryfallCards? =
            listCards(cardsSearchPath)
        while(null != scryfallCard && scryfallCard.has_more){
            val cards = scryfallCard.data
            if(load_in_cache)cards.forEach { ScryfallCardCache.writeToCache(it) }
            allCards.addAll(cards)
            scryfallCard = listCards(scryfallCard.next_page)
        }
        return allCards.toList()
    }
    /**
     * Please consult https://scryfall.com/docs/api/cards/all.
     * @param page Result page to fetch.  Defaults to `1`.
     * @param load_in_cache Whether or not to load results in cache. Defaults to `true`.
     */
    fun getCards(page: Int = 1, load_in_cache: Boolean = true): List<ScryfallCard> {
        val url = "$cardsPath?page=$page"
        val response: Response = getResponse(url)
        val responseJson: JSONObject = response.jsonObject
        val cards: ScryfallCards? =
            ScryfallCardJsonUtility.deserializeScryfallCards(responseJson.toString())
        if(load_in_cache && null != cards)cards.data.forEach { ScryfallCardCache.writeToCache(it) }
        return if (null != cards)cards.data.toList()
        else listOf()
    }

    /**
     * Please consult https://scryfall.com/docs/api/cards/search.
     * @param q Fulltext search query.
     * @param unique The strategy for omitting similar cards.
     *  <ul>
     *      <li>cards - Removes duplicate gameplay objects (cards that share a name and have the same functionality). This is the default</li>
     *      <li>art - Returns only one copy of each unique artwork for matching cards. </li>
     *      <li>prints - Returns all prints for all cards matched (disables rollup).</li>
     *  </ul>
     * @param order Determines how Scryfall should sort the returned cards.
     *  <ul>
     *     <li> name - Sort cards by name, A → Z. This is the default.</li>
     *     <li>set - Sort cards by their set and collector number: AAA/#1 → ZZZ/#999</li>
     *     <li>released - Sort cards by their release date: Newest → Oldest</li>
     *     <li>rarity - Sort cards by their rarity: Common → Mythic</li>
     *     <li>color - Sort cards by their color and color identity: WUBRG → multicolor → colorless</li>
     *     <li>usd - Sort cards by their lowest known U.S. Dollar price: 0.01 → highest, null last</li>
     *     <li>tix - Sort cards by their lowest known TIX price: 0.01 → highest, null last</li>
     *     <li>eur - Sort cards by their lowest known Euro price: 0.01 → highest, null last</li>
     *     <li>cmc - Sort cards by their converted mana cost: 0 → highest</li>
     *     <li>power - Sort cards by their power: null → highest</li>
     *     <li>toughness - Sort cards by their toughness: null → highest</li>
     *     <li>edhrec - Sort cards by their EDHREC ranking: lowest → highest</li>
     *     <li>artist - Sort cards by their front-side artist name: A → Z</li>
     *  </ul>
     * @param dir The direction to sort cards
     *  <ul>
     *      <li>auto - Scryfall will automatically choose the most inuitive direction to sort. This is the default.
     *      <li>asc - Sort ascending (the direction of the arrows in the previous table)
     *      <li>desc - Sort descending (flip the direction of the arrows in the previous table
     *  </ul>
     * @param include_extras If true, extra cards (tokens, planes, etc) will be included. Default value is `false`.
     * @param include_multilingual If true, cards in every language supported by Scryfall will be included. Defaults to `false`.
     * @param include_variations If true, rare care variants will be included, like the Hairy Runesword. Defaults to `false`.
     * @param page The page number to return, default `1`.
     */

    fun search(q: String, unique: UniquenessStrategy = UniquenessStrategy.cards, order: Order = Order._name, dir: Direction = Direction.auto, include_extras: Boolean = false,
               include_multilingual: Boolean = false, include_variations: Boolean = false, page: Int = 1): List<ScryfallCard> {
        val orderValue: String = if(Order._name.equals(order))"name" else order.name
        val url = buildUrl(
            cardsSearchPath, ParameterTuple("q", q),
            ParameterTuple("unique", unique.name),
            ParameterTuple("order", orderValue),
            ParameterTuple("dir", dir.name),
            ParameterTuple("include_extras", include_extras),
            ParameterTuple("include_multilingual", include_multilingual),
            ParameterTuple("include_variations", include_variations),
            ParameterTuple("page", page)
        )
        val response: Response =  getResponse(url)
        val responseJson: JSONArray = response.jsonArray
        return responseJson.map { ScryfallCardJsonUtility.deserializeScryfallCard(it.toString()) }
            .filterNotNull()
            .toList()
    }

    /**
     * Please consult https://scryfall.com/docs/api/cards/named. Only specifying the search type is supported.
     * @param name Card name to search for
     * @param searchType Type or search to conduct:
     *  `<ol>
     *      <li>exact - The exact card name to search for, case insenstive. This is the default value.
     *      <li>fuzzy - A fuzzy card name to search for.
     *  </ol>
     *  @param find_in_cache Whether or not to look in the cache before submitting request to Scryfall. Defaults to `true`.
     */
    fun searchNamed(name: String, searchType: SearchType = SearchType.exact, find_in_cache: Boolean = true) : ScryfallCard? =
        getCardIfNotInCache(name,"$cardsPath/named?${searchType.name}=${encode(name)}", { card -> card!!.name},find_in_cache)


    /**
     * Please consult https://scryfall.com/docs/api/cards/random
     * @param q An optional fulltext search query to filter the pool of random card.
     */
    fun random(q: String = ""): ScryfallCard? {
        val url: String = if(q.isNotEmpty())"$randomCardPath?q=${encode(q)}" else randomCardPath
        val response: Response = getResponse(url)
        return ScryfallCardJsonUtility.deserializeScryfallCard(response.jsonObject.toString())

    }

    /**
     * Please consult https://scryfall.com/docs/api/cards/autocomplete
     * @param q The string to autocomplete
     * @param find_in_cache Whether or not to look in the cache before submitting request to Scryfall. Defaults to `true`.
     */
    fun autoComplete(q: String, find_in_cache: Boolean = true): AutoCompletionCatlogue? {
        var autoCompletionCatlogue: AutoCompletionCatlogue? =
            if(find_in_cache) {
                val entry: AutoCompletionCatlogueEntry? = AutocmopletionCatlogueEntryCache.findInCache(q){ autoCompletionCatlogueEntry -> autoCompletionCatlogueEntry!!.name}
                if(null!= entry)entry.catalog
                else null
            }
            else null
        if(null == autoCompletionCatlogue){
            val url = "$autoCompletionPath?q=${encode(q)}"
            val response: Response = getResponse(url)
            val attempt: Attempt<AutoCompletionCatlogue> = Attempt{
                ScryfallCardJsonUtility.deserialzeAutocomplettionCatalogue(response.jsonObject.toString())!!
            }
            if(attempt.succeeded()){
                autoCompletionCatlogue = attempt.get()
                AutocmopletionCatlogueEntryCache.writeToCache(AutoCompletionCatlogueEntry(q, attempt.get()))
            }

        }
        return autoCompletionCatlogue

    }

    /**
     * Please consult https://scryfall.com/docs/api/cards/collector
     * @param code The three to five-letter set code.
     * @param number The collector number
     * @param lang The 2-3 character language code. Defaults to 'en'.
     * @param find_in_cache Whether or not to look in the cache before submitting request to Scryfall. Defaults to `true`.
     */
    fun collector(code: String, number: Int, lang: String = "en", find_in_cache: Boolean = true): ScryfallCard? =
        getCardIfNotInCache("$code:$number:$lang", "$cardsPath/$code/$number/$lang",
            {card -> "${card!!.set}:${card.collector_number}:${card.lang}"},
            find_in_cache)


    /**
     * Please consult https://scryfall.com/docs/api/cards/id
     * @param id The Scryfall Id
     * @param find_in_cache Whether or not to look in the cache before submitting request to Scryfall. Defaults to `true`.
     */
    fun getCardById(id: String, find_in_cache: Boolean = true): ScryfallCard? =
        getCardIfNotInCache(id,"$cardsPath/${encode(id)}", { card -> card!!.id}, find_in_cache)

    /**
     * Please consult https://scryfall.com/docs/api/cards/tcgplayer
     * @param id The TGC Player Id
     * @param find_in_cache Whether or not to look in the cache before submitting request to Scryfall. Defaults to `true`.
     */
    fun getCardByTcgPlayerId(id: Int, find_in_cache: Boolean = true): ScryfallCard? =
        getCardIfNotInCache(id, "$tgcpCardSearchPath/$id", {card -> card!!.tcgplayer_id}, find_in_cache)

    /**
     * Please consult https://scryfall.com/docs/api/cards/arena
     * @param id The Arena Id
     * @param find_in_cache Whether or not to look in the cache before submitting request to Scryfall. Defaults to `true`.
     */
    fun getCardByArenaId(id: Int, find_in_cache: Boolean = true): ScryfallCard? =
        getCardIfNotInCache(id, "$areaCardSearchPath/$id", { card -> card!!.arena_id}, find_in_cache)

    /**
     * Please consult https://scryfall.com/docs/api/cards/mtgo
     * @param id The MTGO Id
     * @param find_in_cache Whether or not to look in the cache before submitting request to Scryfall. Defaults to `true`.
     */
    fun getCardByMtgoId(id: Int, find_in_cache: Boolean = true): ScryfallCard? =
        getCardIfNotInCache(id, "$mtgoCardSearchPath/$id", { card -> card!!.mtgo_id}, find_in_cache)

    /**
     * Please consult https://scryfall.com/docs/api/cards/mtgo
     * @param id The Multiverse Id
     * @param find_in_cache Whether or not to look in the cache before submitting request to Scryfall. Defaults to `true`.
     */
    fun getCardByMultiverseId(id: Int, find_in_cache: Boolean = true): ScryfallCard? =
        getCardIfNotInCache(arrayOf(id), "$multiverseCardSearchPath/$id", { card -> card!!.multiverse_ids}, find_in_cache)


    private fun <V>getCardIfNotInCache(searchTerm: V, fallbackUrl: String,
                                       comparisonSupplier: (ScryfallCard?) -> V,
                                       find_in_cache: Boolean): ScryfallCard? {
        var card = if(find_in_cache)
            ScryfallCardCache.findInCache(searchTerm, comparisonSupplier)
        else null
        if(null == card){
            val response: Response = getResponse(fallbackUrl)
            val attempt: Attempt<ScryfallCard?>
            attempt = Attempt{
                ScryfallCardJsonUtility.deserializeScryfallCard(response.jsonObject.toString())
            }
            if(attempt.succeeded()){
                card = attempt.get()
                ScryfallCardCache.writeToCache(card!!)
            }
        }
        return card
    }

    private fun listCards(url: String): ScryfallCards? {
        val response: Response = getResponse(url)
        val responseJson = response.jsonObject
        return ScryfallCardJsonUtility.deserializeScryfallCards(responseJson.toString())
    }

    private fun getResponse(url: String): Response {
        println("Making call to $url")
        return khttp.get(
            url = url,
            headers = Constants.headers)
    }

}