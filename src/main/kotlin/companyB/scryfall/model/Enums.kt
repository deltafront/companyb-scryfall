package companyB.scryfall.model

/**
 * Enumerated values for Image URIs.
 */
enum class ImageUri{
    small, normal, large, png, art_crop, border_crop
}

/**
 * Enumerated values for Rulings.
 */
enum class Ruling{
    not_legal, legal, restricted, banned
}

/**
 * Enumerated values for Purchase URIs.
 */
enum class PurchaseUri {
    tcgplayer, cardmarket, cardhoarder
}

/**
 * Enumerated values for Prices.
 */
enum class Price {
    usd, usd_foil,eur,tix
}

/**
 * Enumerated values for Related URIs.
 */
enum class RelatedUri{
    gatherer, tcgplayer_decks, edrec, mtgtop8
}

/**
 * Enumerated values for Game Types
 */
enum class GameType {
    standard, future, modern, legacy, historic, pioneer,
    pauper, vintage, penny, commander, brawl, duel,oldschool
}

/**
 * Enumerated values for Games
 */
enum class Games{
    mtgo,paper,arena
}

/**
 * Enumerated values for Rarity.
 */
enum class Rarity{
    common,uncommon,rare,mythic
}