package companyB.scryfall.model

import companyB.scryfall.utility.ComparisonUtility
import companyB.scryfall.utility.HashCodeUtility

data class ScryfallCards(val `object`: String,
                         val total_cards: Long,
                         val has_more: Boolean,
                         val next_page: String,
                         val data: Array<ScryfallCard>,
                         val warnings: Array<String> = arrayOf()){
    override fun equals(other: Any?): Boolean = ComparisonUtility.compareValues(this,other!!)
    override fun hashCode(): Int  = HashCodeUtility.hashCode(this)
}

data class ScryfallCard(val name: String,
                        val printed_name: String = "",
                        val `object`: String,
                        val id: String,
                        val oracle_id: String,
                        val multiverse_ids: Array<Int>,
                        val mtgo_id: Int= -1,
                        val arena_id: Int = -1,
                        val mtgo_foil_id: Int = -1,
                        val tcgplayer_id: Int = -1,
                        val lang: String,
                        val uri: String,
                        val scryfall_uri: String,
                        val layout: String,
                        val released_at: String,
                        val image_uris: Map<String,String> = mapOf(),
                        val highres_image: Boolean = false,
                        val mana_cost: String = "",
                        val cmc: Double,
                        val type_line: String,
                        val oracle_text: String = "",
                        val colors: Array<String> = arrayOf(),
                        val color_identity: Array<String>,
                        val games: Array<String>,
                        val reserved: Boolean,
                        val foil: Boolean,
                        val nonfoil: Boolean,
                        val oversized: Boolean,
                        val promo: Boolean,
                        val reprint: Boolean,
                        val variation: Boolean,
                        val set: String,
                        val set_name: String,
                        val set_type: String,
                        val set_search_uri: String,
                        val scryfall_set_uri: String,
                        val rulings_uri: String,
                        val prints_search_uri: String,
                        val collector_number: String,
                        val digital: Boolean,
                        val rarity: String,
                        val artist: String,
                        val artist_ids: Array<String>,
                        val illustration_id: String = "",
                        val card_back_id: String,
                        val border_color: String,
                        val frame: String,
                        val full_art: Boolean,
                        val textless: Boolean,
                        val booster: Boolean,
                        val story_spotlight: Boolean,
                        val edhrec_rank: Int = -1,
                        val prices: Map<String, String>,
                        val related_uris: Map<String, String>,
                        val purchase_uris: Map<String, String>,
                        val legalities: Map<String,String> = mapOf(),
                        val power: String = "",
                        val toughness: String = ""){
    override fun equals(other: Any?): Boolean = ComparisonUtility.compareValues(this,other!!)
    override fun hashCode(): Int  = HashCodeUtility.hashCode(this)
}


data class AutoCompletionCatlogueEntry(val name: String, val catalog: AutoCompletionCatlogue)

data class AutoCompletionCatlogue(val `object`: String, val total_values: Int, val data: Array<String>){
    override fun equals(other: Any?): Boolean = ComparisonUtility.compareValues(this,other!!)
    override fun hashCode(): Int  = HashCodeUtility.hashCode(this)
}

data class Catalogue(val `object`: String, val total_values: Int, val data: Array<String>, val uri: String)