package companyB.scryfall.cache

import companyB.scryfall.Constants.autoCompletionCacheFile
import companyB.scryfall.Constants.cardCacheFile
import companyB.scryfall.model.AutoCompletionCatlogueEntry
import companyB.scryfall.model.Catalogue
import companyB.scryfall.model.ScryfallCard
import companyB.scryfall.utility.ScryfallCardJsonUtility
import java.io.File
import java.io.FileReader
import java.io.FileWriter

object ScryfallCardCache : BaseCache<ScryfallCard>(cardCacheFile, ScryfallCardJsonUtility::deserializeScryfallCard,
    ScryfallCardJsonUtility::serializeScryfallCard)

object AutocmopletionCatlogueEntryCache: BaseCache<AutoCompletionCatlogueEntry>(autoCompletionCacheFile,ScryfallCardJsonUtility::deserialzeAutocomplettionCatalogueEntry,
    ScryfallCardJsonUtility::serializeAutocmopletionCatlogueEntry)

object CatalogueEntryCache: BaseCache<Catalogue>(autoCompletionCacheFile,ScryfallCardJsonUtility::deserializeCatlogue,
    ScryfallCardJsonUtility::serializeCatlogue)



abstract class BaseCache<T>(val cacheLocation: String, val deserializationFunction: (String) -> T?,
                            val serializationFunction: (T) -> String) {

    private val cache: MutableSet<T?> = loadCache()
    private val newItems: MutableSet<T> = mutableSetOf()

    fun writeToCache(item: T) = newItems.add(item)

    fun<V>findInCache(value: V, supplier: (T?) -> V): T? {
        if(value is Array<*>) return cache.find { item ->
            val array: Array<*> = supplier(item) as Array<*>
            array.contains(value[0])
        }
            else return cache.union(newItems).find { item -> supplier(item)!!.equals(value)}
    }


    fun flushCache(){
        val oldSize = cache.size
        cache.addAll(newItems)
        val newSize = cache.size
        if(oldSize != newSize) {
            FileWriter(getFile(cacheLocation)).use { writer ->
                cache.forEach{writer.write("$serializationFunction($it)\n")} }
        }
    }

    fun cacheFileName(): String = cacheLocation

    private fun loadCache(): MutableSet<T?> {
        val file: File = getFile(cacheLocation)
        return if (file.exists()) FileReader(file)
            .readLines()
            .map { line -> deserializationFunction(line) }
            .filterNot { null == it }
            .toMutableSet()
        else mutableSetOf()
    }

    private fun getFile(fileName: String): File {
        return File(fileName)
    }
}