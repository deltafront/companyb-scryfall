package companyB.scryfall.utilty.json

import companyB.scryfall.model.AutoCompletionCatlogue
import companyB.scryfall.model.Catalogue
import companyB.scryfall.model.ScryfallCard
import companyB.scryfall.model.ScryfallCards
import companyB.scryfall.utility.ScryfallCardJsonUtility
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.Test
import kotlin.test.assertEquals
import kotlin.test.assertNotNull

class ScryfallJsonUtilityDeserializeTests : ScryfallJsonUtilityTestBase(){

    @Test
    fun deserializeCard(){
        val card: ScryfallCard? = getObject("single-card", ScryfallCardJsonUtility::deserializeScryfallCard)
        assertNotNull(card)
        assertEquals("Austere Command", card.name)
    }

    @Test
    fun deserializeCatalogEntry(){
        val catalogue: Catalogue? = getObject("catalog-entry", ScryfallCardJsonUtility::deserializeCatlogue)
        assertNotNull(catalogue)
        assertEquals("https://api.scryfall.com/catalog/artist-names", catalogue.uri)
        assertEquals(727, catalogue.total_values)
        assertEquals(727, catalogue.data.size)
    }

    @Test
    fun deserializeAutocomplete(){
        val autoCompletionCatlogue: AutoCompletionCatlogue? =
            getObject("autocomplete", ScryfallCardJsonUtility::deserialzeAutocomplettionCatalogue)
        assertNotNull(autoCompletionCatlogue)
        assertEquals(20, autoCompletionCatlogue.total_values)
        assertEquals(20, autoCompletionCatlogue.data.size)
    }

    @Test
    fun deserializeCards(){
        val cards: ScryfallCards?  = getObject("multiple-cards", ScryfallCardJsonUtility::deserializeScryfallCards)
        assertNotNull(cards)
        assertEquals(261204, cards.total_cards)
        assertEquals(175, cards.data.size)
        assertTrue(cards.has_more)
        assertEquals("https://api.scryfall.com/cards?page=4", cards.next_page)
    }

}