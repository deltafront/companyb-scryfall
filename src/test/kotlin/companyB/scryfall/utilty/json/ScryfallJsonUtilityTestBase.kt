package companyB.scryfall.utilty.json

import companyB.scryfall.ScryfallTestBase
import java.io.FileReader

abstract class ScryfallJsonUtilityTestBase: ScryfallTestBase() {

    protected fun <T>getObject(filepath: String, serializerFunction: (String) ->T): T {
        val finalFilePath = "${getTestResourceBase()}/$filepath.json"
        val jsonString = StringBuffer()
        FileReader(finalFilePath).use { jsonString.append(it.readText()) }
        return serializerFunction(jsonString.toString())
    }
}