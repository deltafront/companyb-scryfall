package companyB.scryfall.utilty.comparison

import companyB.scryfall.ScryfallTestBase
import companyB.scryfall.model.ScryfallCard
import companyB.scryfall.model.ScryfallCards
import companyB.scryfall.utility.ComparisonUtility
import companyB.scryfall.utility.ScryfallCardJsonUtility
import org.junit.jupiter.api.Test
import java.io.FileReader
import kotlin.test.assertEquals

class ComparisonUtilityTests : ScryfallTestBase(){

    @Test
    fun shouldEqual(){
        runTest(2,2)
    }
    @Test
    fun shouldNotEqual(){
        runTest(2, 5)
    }


    private fun runTest(index1:Int, index2: Int) : Unit {
        val jsonString = FileReader("${getTestResourceBase()}/multiple-cards.json").use { it.readText() }
        val scryfallCards: ScryfallCards? = ScryfallCardJsonUtility.deserializeScryfallCards(jsonString)
        val data: Array<ScryfallCard> = scryfallCards!!.data
        val card1: ScryfallCard = data[index1]
        val card2: ScryfallCard = data[index2]
        val actualEquality: Boolean = ComparisonUtility.compareValues(card1, card2)
        assertEquals(actualEquality, index1 == index2)

    }
}