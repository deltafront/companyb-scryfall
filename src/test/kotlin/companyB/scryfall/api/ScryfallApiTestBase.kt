package companyB.scryfall.api

import companyB.scryfall.Constants
import org.junit.jupiter.api.AfterAll
import org.junit.jupiter.api.TestInstance
import java.io.File
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
open class ScryfallApiTestBase {

    @AfterAll
    fun afterAll(){
        fun deleteFile(filename: String) {
            val file = File(filename)
            if(file.exists())file.delete()
        }
        listOf(
            Constants.autoCompletionCacheFile,
            Constants.cardCacheFile,
            Constants.catalogCacheFile
        )
            .forEach{deleteFile(it)}
    }
}