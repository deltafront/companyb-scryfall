package companyB.scryfall.api

import companyB.scryfall.model.ScryfallCard
import org.junit.jupiter.api.Assertions.assertNull
import org.junit.jupiter.api.Test
import kotlin.test.assertNotNull
import kotlin.test.assertTrue

class ScryfallCardsApiTest: ScryfallApiTestBase() {
    val api: ScryfallCardsApi = ScryfallCardsApi

    @Test
    fun getNamedCardExact() {
        assertNotNull(api.searchNamed("Ghostly Flicker", SearchType.exact))
    }

    @Test
    fun getInvalidCard() {
        assertNull(api.searchNamed("FooBarrio", SearchType.exact))
    }

    @Test
    fun getNamedCardFuzzy() {
        assertNotNull(api.searchNamed("Ghos Flick", SearchType.fuzzy))
    }

    @Test
    fun getRandomCardNoText() {
        assertNotNull(api.random())
    }

    @Test
    fun getRandomCardWitText() {
        assertNotNull(api.random("Exile"))
    }

    @Test
    fun getById() {
        assertNotNull(api.getCardById("fccd7eda-5c10-4aa7-a70c-b52f685972e4"))
    }

    @Test
    fun getByMtgoId() {
        assertNotNull(api.getCardByMtgoId(63331))
    }

    @Test
    fun getByMultiverseId() {
        assertNotNull(api.getCardByMultiverseId(425864))
    }

    @Test
    fun getByTcgPlayerId() {
        assertNotNull(api.getCardByTcgPlayerId(128877))
    }

    @Test
    fun getByArenaId() {
        assertNotNull(api.getCardByArenaId(67224))
    }

    @Test
    fun getByCollector() {
        assertNotNull(api.collector("dom", 60))
    }

    @Test
    fun getCards() {
        val cards: List<ScryfallCard> = api.getCards()
        assertNotNull(cards)
        assertTrue(cards.isNotEmpty())
    }

    @Test
    fun getAutocomplete(){
        assertNotNull(api.autoComplete("cat"))
    }

    //TODO
    fun search(){

    }

}