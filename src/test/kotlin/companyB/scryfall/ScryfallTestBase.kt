package companyB.scryfall

open class ScryfallTestBase {

    fun getTestResourceBase(): String = "${System.getProperty("user.dir")}/src/test/resources"
}