package companyB.scryfall.cache

import companyB.scryfall.model.Catalogue
import companyB.scryfall.utility.ScryfallCardJsonUtility
import org.junit.jupiter.api.Test

class ScryfallCatalogCacheTest: ScryfallCacheTestBase<Catalogue, String>() {
    override fun deserializer(): (String) -> Catalogue? =  ScryfallCardJsonUtility::deserializeCatlogue

    override fun cache(): BaseCache<Catalogue>  = CatalogueEntryCache

    @Test
    fun getCatalog(){
        testCache("catalog-entry.json", "https://api.scryfall.com/catalog/artist-names"){catalogue -> catalogue!!.uri }
    }
}