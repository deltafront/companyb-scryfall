package companyB.scryfall.cache

import companyB.scryfall.model.ScryfallCard
import companyB.scryfall.utility.ScryfallCardJsonUtility
import org.junit.jupiter.api.Test

class ScryfallCardsCacheTest : ScryfallCacheTestBase<ScryfallCard,String>() {

    override fun deserializer(): (String) -> ScryfallCard?  = ScryfallCardJsonUtility::deserializeScryfallCard
    override fun cache(): BaseCache<ScryfallCard>  = ScryfallCardCache

    @Test
    fun getByName(){
        testCache("single-card.json", "Austere Command"){card -> card!!.name}
    }

}