package companyB.scryfall.cache

import companyB.scryfall.ScryfallTestBase
import companyB.scryfall.model.AutoCompletionCatlogue
import companyB.scryfall.model.AutoCompletionCatlogueEntry
import companyB.scryfall.utility.ComparisonUtility
import companyB.scryfall.utility.ScryfallCardJsonUtility
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import java.io.File
import java.io.FileReader

class ScryfallAutoCompleteCacheTest: ScryfallTestBase(){

    @Test
    fun getAutoCompletionCatalog(){
        val finalFilename = "${getTestResourceBase()}/autocomplete.json"
        val cache: BaseCache<AutoCompletionCatlogueEntry>  = AutocmopletionCatlogueEntryCache
        val identifier = "thall"
        val cacheSupplierFunction: (AutoCompletionCatlogueEntry?) -> String = { autoCompletionCatlogueEntry -> autoCompletionCatlogueEntry!!.name }
        try{
            val nullResultFromCache = cache.findInCache(identifier, cacheSupplierFunction)
            Assertions.assertNull(nullResultFromCache)
            val input: String = FileReader(File(finalFilename)).use { it.readText() }
            val valueFromFile: AutoCompletionCatlogue? = ScryfallCardJsonUtility.deserialzeAutocomplettionCatalogue(input)
            cache.writeToCache(AutoCompletionCatlogueEntry(identifier, valueFromFile!!))
            val notNullValueFromCache: AutoCompletionCatlogueEntry? = cache.findInCache(identifier, cacheSupplierFunction)
            Assertions.assertNotNull(notNullValueFromCache)
            Assertions.assertTrue(ComparisonUtility.compareValues(valueFromFile, notNullValueFromCache!!.catalog))
        }
        finally {
            File(cache.cacheLocation).delete()
        }
    }
}