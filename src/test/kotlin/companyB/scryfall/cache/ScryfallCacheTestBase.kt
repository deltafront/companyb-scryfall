package companyB.scryfall.cache

import companyB.scryfall.ScryfallTestBase
import companyB.scryfall.utility.ComparisonUtility
import org.junit.jupiter.api.Assertions.*
import java.io.File
import java.io.FileReader

abstract class ScryfallCacheTestBase<T,V> : ScryfallTestBase(){

    abstract fun deserializer(): (String) -> T?
    abstract fun cache(): BaseCache<T>

    fun testCache(filename: String,
                        identifier: V,
                   cacheSupplierFunction: (T?) -> V) {
        val finalFilename = "${getTestResourceBase()}/$filename"
        val cache: BaseCache<T> = cache()
        try{
            val nullResultFromCache: T? = cache.findInCache(identifier, cacheSupplierFunction)
            assertNull(nullResultFromCache)
            val valueFromFile: T = getDeserializedObject(finalFilename, deserializer())!!
            cache.writeToCache(valueFromFile)
            val notNullValueFromCache: T? = cache.findInCache(identifier, cacheSupplierFunction)
            assertNotNull(notNullValueFromCache)
            assertTrue(ComparisonUtility.compareValues(valueFromFile!!, notNullValueFromCache!!))
        }
        finally {
            File(cache.cacheLocation).delete()
        }

    }

    private fun getDeserializedObject(filename: String, deserializer: (String) -> T?): T? {
        val input: String = FileReader(File(filename)).use { it.readText() }
        return deserializer(input)
    }
}